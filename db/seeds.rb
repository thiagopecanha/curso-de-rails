require 'factory_girl'
load 'test/factories/posts.rb'
load 'test/factories/comentarios.rb'
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
10.times do
  post = FactoryGirl.create(:post)
  10.times do
    comentario = FactoryGirl.create(:comentario, post_id: post.id)
  end
end