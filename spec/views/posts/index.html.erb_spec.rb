require 'spec_helper'

describe "posts/index" do
  before(:each) do
    2.times do
        post = FactoryGirl.create(:post)
      end
    @posts = Post.paginate(:page => params[:page] || 1, :per_page => params[:per_page] || 10)
    
    @posts.stub(:limit_value).and_return 2

    assign(:posts, @posts)

  end

  it "renders a list of posts" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => @posts.first.titulo.to_s, :count => 1
    assert_select "tr>td", :text => @posts.first.conteudo.to_s, :count => 1
  end
end
