class Post
  include MongoMapper::Document

  key :titulo, String, :required=>true
  key :conteudo, String
  
  many :comentarios
end
